#include "GameRoom.h"

GameRoom::GameRoom(){
    //ctor
}

GameRoom::~GameRoom(){
    //dtor
}

void GameRoom::insertInTeamOne(Player* _player){

}

void GameRoom::insertInTeamTwo(Player* _player){

}

vector<Player*> GameRoom::getTeamOne(){
    return teamOne;
}

vector<Player*> GameRoom::getTeamTwo(){
    return teamTwo;
}


void GameRoom::insertInTeam(Player* _player){
    if(teamOne.size() <= teamTwo.size()){
        _player->setStatus(statusID::TeamOne);
        teamOne.push_back(_player);
    }
    else{
        if(teamTwo.size() <= 3){
            _player->setStatus(statusID::TeamTwo);
            teamTwo.push_back(_player);
        }

    }
    return;
}

void GameRoom::removeFromTeam(int playerID){

    int n = teamOne.size();

    for(int i = 0; i < n; i++){
        if(teamOne.at(i)->getID() == playerID){
            teamOne.erase(teamOne.begin() + i);
            return;
        }
    }

    int m = teamTwo.size();

    for(int i = 0; i < m; i++){
        if(teamTwo.at(i)->getID() == playerID){
            teamTwo.erase(teamTwo.begin() + i);
            return;
        }
    }
}

vector<Player*> GameRoom::getPlayers(){
    std::vector<Player*> players;
    players.reserve( teamOne.size() + teamTwo.size() ); // preallocate memory
    players.insert( players.end(), teamOne.begin(), teamOne.end());
    players.insert( players.end(), teamTwo.begin(), teamTwo.end());

    return players;
}
