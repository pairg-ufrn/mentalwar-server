#include "configurationDAO.h"


ConfigurationDAO& ConfigurationDAO::getInstance(){
    static ConfigurationDAO instance;
    return instance;
}

void ConfigurationDAO::loadConfiguration(){
    std::ifstream config {"mentalward.conf"};
    parse(config);
    port = Utility::StringToNumber(options["Port"]);

    logDirectory = options["LogDirectory"];
    dataDirectory = options["DataDirectory"];
}

void ConfigurationDAO::parse(std::ifstream & cfgfile){
    std::string id, eq, val;

    while(cfgfile >> id >> val){
      if (id[0] == '#') continue;  // skip comments
      //if (eq != "=") throw std::runtime_error("Parse error");

      options[id] = val;
    }
}

std::string ConfigurationDAO::getDataDirectory(){
    return dataDirectory;
}

std::string ConfigurationDAO::getLogDirectory(){
    return logDirectory;
}

int ConfigurationDAO::getPort(){
    return port;
}
