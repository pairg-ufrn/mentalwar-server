#include "match.h"

Match::Match(std::vector<Player*> _teamOne, std::vector<Player*> _teamTwo){

    teamOne.reserve(_teamOne.size());
    teamOne.insert(teamOne.end(), _teamOne.begin(), _teamOne.end());

    teamTwo.reserve(_teamTwo.size());
    teamTwo.insert(teamTwo.end(), _teamTwo.begin(), _teamTwo.end());

    this->startTime = Utility::getDatestamp(false, true);

    durationClock.restart();

    status = 1;

}

Match::~Match(){

}

std::vector<Player*> Match::getTeamOne(){
    return this->teamOne;
}


std::vector<Player*> Match::getTeamTwo(){
    return this->teamTwo;
}

std::vector<Player*> Match::getPlayers(){
    std::vector<Player*> players;
    players.reserve( teamOne.size() + teamTwo.size() ); // preallocate memory
    players.insert( players.end(), teamOne.begin(), teamOne.end());
    players.insert( players.end(), teamTwo.begin(), teamTwo.end());

    return players;
}

int Match::getID(){
    return this->ID;
}

int Match::getLoser(){
    return this->loser;
}

int Match::getWinner(){
    return this->winner;
}


int Match::getStatus(){
    return this->status;
}

void Match::setWinner(int winner){
    this->winner = winner;
}

void Match::setLoser(int loser){
    this->loser = loser;
}

int Match::getDuration(){
    return static_cast<int>(durationClock.getElapsedTime().asSeconds());
}

float Match::getTeamOneAverageAttention(){

    float avg = 0.f;

    int n = getTeamOne().size();

    for(auto const& player : getTeamOne()){
        avg += player->getAverageAttention();
    }

    n > 0 ? avg /= n : avg = 0;

    return avg;
}

float Match::getTeamTwoAverageAttention(){
    float avg = 0.f;

    int n = getTeamTwo().size();

    for(auto const& player : getTeamTwo()){
        avg += player->getAverageAttention();
    }

    n > 0 ? avg /= n : avg = 0;

    return avg;
}


void Match::resetPlayers(){
    for(auto& player : getPlayers()){
        player->reset();
    }
}
