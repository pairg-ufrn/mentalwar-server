#include <SFML/Network.hpp>
#include <SFML/System.hpp>

#include <memory>
#include <omp.h>
#include <iostream>

#include <stdexcept>
#ifdef __cplusplus__
  #include <cstdlib>
#else
  #include <stdlib.h>
#endif

#include "connectionHandler.h"


int main(){

    connectionHandler.run();

}
