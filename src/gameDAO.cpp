#include "gameDAO.h"

void GameDAO::logMatch(Match* match){
    ofstream output {configurationDAO.getLogDirectory() + "daemon/daemon_" + Utility::getDatestamp("") + ".log", ios::app};

    string leftnames;
    string rightnames;

    for(auto& player : match->getTeamOne()){
        leftnames += player->getNickname() + " ";
    }

    for(auto& player : match->getTeamTwo()){
        rightnames += player->getNickname() + " ";
    }

    output  <<  Utility::getTimestamp()  << " initializing match: "
            << " " << leftnames  << ""
            << " " << rightnames << "\n";

}

void GameDAO::logPlayers(Match* match){
    for(auto& player : match->getTeamOne()){
        ofstream playerOutput {configurationDAO.getDataDirectory() + "players/"
                                + player->getNickname()
                                + "_"
                                + Utility::getYear() + Utility::getMonth() + Utility::getDay()
                                + "_"
                                + Utility::getTimestamp("")
                                + ".log", ios::app};

        playerOutput << "Nickname: "         << player->getNickname() << "\n"
                    << "Team: one"         << "\n"
                    << "Average Attention:" << player->getAverageAttention() << "\n\n"
                    << "Values:\n";

        for(auto& value : player->getAttentionValues()){
            playerOutput << value << "\n";
        }
    }

    for(auto& player : match->getTeamTwo()){
        ofstream playerOutput {configurationDAO.getDataDirectory()
                                + "players/"
                                + player->getNickname()
                                + "_"
                                + Utility::getYear() + Utility::getMonth() + Utility::getDay()
                                + "_"
                                + Utility::getTimestamp("")
                                + ".log", ios::app};

        playerOutput << "Nickname: "         << player->getNickname() << "\n"
                     << "Team: two"          << "\n"
                     << "Average Attention:" << player->getAverageAttention() << "\n\n"
                     << "Values:\n";

        for(auto& value : player->getAttentionValues()){
            playerOutput << value << "\n";
        }
    }
}

void GameDAO::saveMatch(Match* match){
    ofstream matchOutput {configurationDAO.getDataDirectory() + "matches/match_" + Utility::getYear() + Utility::getMonth() + Utility::getDay() + "_" + Utility::getTimestamp("") + ".log", ios::app};

    matchOutput << "Duration: " << match->getDuration() << " seconds\n\n";

    for(auto& player : match->getTeamOne()){
        matchOutput << "Nickname: "         << player->getNickname() << "\n"
                    << "Team: one"         << "\n"
                    << "Average Attention:" << player->getAverageAttention() << "\n\n";
    }

    for(auto& player : match->getTeamTwo()){
        matchOutput << "Nickname: "         << player->getNickname() << "\n"
                    << "Team: two"        << "\n"
                    << "Average Attention:" << player->getAverageAttention() << "\n\n";
    }
}

