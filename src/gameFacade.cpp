#include "gameFacade.h"

void GameFacade::handleRequest(sf::Packet packet, int pktID, int connectionID){

    switch(static_cast<packetID> (pktID) ){

        case packetID::GameRequest: {

           // gameControl.gameRequest(playerID, gameMode, gameTime);
            break;
        }

        case packetID::Attention: {
            int attention;

            packet >> attention;

            gameControl.sendAttention(connectionID, attention);

            break;
        }

        case packetID::Chat : {
            string msg;
            packet >> msg;
            gameControl.sendChatMessage(connectionID, msg);
        }

        case packetID::ExitQueue:
            gameControl.removeFromQueue(connectionID);
        break;

    }

}


Player* GameFacade::getPlayer(int ID){
    return gameControl.getPlayer(ID);
}
