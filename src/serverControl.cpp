#include "serverControl.h"

int ServerControl::numberOfOnlineUsers = 0;

ServerState ServerControl::state = ServerState::waiting;

void ServerControl::initialize(){

}


int ServerControl::getNumberOfOnlineUsers(){
    int number = 0;

    return number;
}

void ServerControl::broadcast(std::string message){
    sf::Packet packet;
    Utility::buildPacketHeader(packetID::Broadcast, packet);
    packet << message;

    for(auto& user : connectionHandler.connections){
        user->send(packet);
    }
}


void ServerControl::sendPlayersList(){
    sf::Packet packet;
    Utility::buildPacketHeader(packetID::PlayerList, packet);

    vector<Player*> players = gameControl.getPlayers();
    int lenght = players.size();

    packet << lenght;

    for(auto& player : players){
        packet << player->getID()
               << player->getNickname()
               << static_cast<int>(player->getStatus())
               << player->isReady()
               << player->getHead()
               << player->getBody()
               << player->getLegs()
               << player->getSex();
    }

    for(auto& connected : connectionHandler.connections){
        connected->send(packet);
    }
}


void ServerControl::processLogin(int connectionID, std::string nickname, std::string version, int head, int body, int legs, std::string sex){
    sf::Packet packet;

    if(version == GAME_VERSION){

        Utility::buildPacketHeader(packetID::LoginResponse, packet);

        connectUser( connectionID, nickname, head, body, legs, sex );

        ServerDAO::logHistory ("logon", nickname, connectionID);

        packet << true << connectionID << nickname;

    }
    else{
        Utility::buildPacketHeader(packetID::WrongVersion, packet);
        return;
    }

    Utility::sendPacket(connectionID, packet);

    sendPlayersList();
}

void ServerControl::connectUser(int ID, std::string nickname, int head, int body, int legs, std::string sex){
    connectionHandler.lastLogin = ID;
    numberOfOnlineUsers++;

    Player* newPlayer = new Player(ID, nickname, head, body, legs, sex);

    if(state == ServerState::ongoingMatch){
        gameControl.insertInQueue(newPlayer);
    }
    else{
        gameControl.insertInRoom(newPlayer);
    }
}


void ServerControl::disconnectUser(int ID){

    gameControl.deletePlayer(ID);
    numberOfOnlineUsers--;

    connectionHandler.lastLogout = ID;
    sendPlayersList();
    //ServerDAO::logHistory ("disconnected", user->getUsername(), user->getConnectionID());
}


void ServerControl::notifyLogin(std::string nickname){
    sf::Packet packet;
    Utility::buildPacketHeader(packetID::PlayerList, packet);

    for(auto& connected : connectionHandler.connections){
        connected->send(packet);
    }
}


void ServerControl::disconnectOfflineUsers(){

}
