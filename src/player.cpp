#include "player.h"


int Player::getID(){
    return ID;
}

std::string Player::getNickname(){
    return nickname;
}

statusID Player::getStatus(){
    return status;
}

bool Player::isReady(){
    return ready;
}
bool Player::hasBlinked(){
    return blinked;
}

void Player::setReady(bool _ready){
    ready = _ready;
}

void Player::setID(int ID){
    this->ID = ID;
}

void Player::setNickname(std::string _nickname){
    this->nickname = _nickname;
}

void Player::setStatus(statusID _status){
    this->status = _status;
}

void Player::setBlinked(const bool& blink){
    blinked = blink;
}

void Player::logBlink(const int& blinkStrength){
    attentionValues.push_back(-1*blinkStrength);
}


float Player::getAverageAttention(){
    float avg = 0.0;
    int i;
    int n = attentionValues.size();

    for(i = 0; i < n; i++){
        if(attentionValues.at(i) > 0){
            avg += attentionValues.at(i);
        }
    }

    n > 0 ? avg /= n : avg = 0;

    return avg;
}

std::vector<int> Player::getAttentionValues(){
    return attentionValues;
}

int Player::getHead(){
    return head;
}

int Player::getBody(){
    return body;
}

int Player::getLegs(){
    return legs;
}

string Player::getSex(){
    return sex;
}


void Player::setAttention(int att){
    attentionValues.push_back(att);
}

void Player::reset(){
    attentionValues.clear();
    blinked = false;
    ready = false;
}

 Player::Player(int _ID, std::string _nickname, int _head, int _body, int _legs, std::string _sex){
    ID = _ID;
    nickname = _nickname;
    head = _head;
    body = _body;
    legs = _legs;
    sex = _sex;
    ready = false;
}
