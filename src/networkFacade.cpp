#include "networkFacade.h"

void NetworkFacade::handleRequest(sf::Packet packet, int connectionID) {
    int i, pktID;

    string movelog;
    string msg;
    packet >> pktID ;

    switch (static_cast<packetID> (pktID)) {

        case packetID::Login: {
            string nickname, version, sex;

            int head, body, legs;

            packet >> nickname >> version >> head >> body >> legs >> sex;

            if(nickname.size() > 0){
                ServerControl::processLogin(connectionID, nickname, version, head, body, legs, sex);
            }
            break;
        }

        case packetID::Attention: {
            int attention;

            packet >> attention;

            gameControl.sendAttention(connectionID, attention);
            break;
        }

        case packetID::Blink: {
            int blinkStrenght;

            packet >> blinkStrenght;

            gameControl.sendBlink(connectionID, blinkStrenght);
            break;
        }

        case packetID::Chat: {
            GameFacade::handleRequest(packet, pktID, connectionID);
            break;
        }

        case packetID::GameReady:{
            bool ready;

            packet >> ready;

            gameControl.setReady(connectionID, ready);
            ServerControl::sendPlayersList();

            break;
        }

        case packetID::GameEnd :{
            GameFacade::handleRequest(packet, pktID, connectionID);

            int winner;

            packet >> winner;

            gameControl.sendGameEnd(winner);
            break;
        }

        case packetID::Disconnect :
            ServerFacade::handleRequest(packet, pktID, connectionID);
        break;


        case packetID::ExitQueue:
            GameFacade::handleRequest(packet, pktID, connectionID);
        break;

        default : { //redirect
            //if (toPlayer < 0 || toPlayer > network.players.size() -1) return;
            //network.players[toPlayer]->send(packet);

            return; //do not update player list
        }

    }


}
