#include "gameControl.h"


GameControl& GameControl::getInstance(){
    static GameControl instance;
    return instance;
}

GameControl::GameControl(){

}

vector<Player*> GameControl::getPlayers(){
    Players.clear();

    std::vector<Player*> roomPlayers = gameRoom.getPlayers();

    Players.reserve( gameQueue.size() + roomPlayers.size() ); // preallocate memory
    Players.insert ( Players.end(), gameQueue.begin(), gameQueue.end());
    Players.insert ( Players.end(), roomPlayers.begin(), roomPlayers.end());
    return Players;
}

void GameControl::deletePlayer(int ID){

    removeFromQueue(ID);

    removeFromRoom(ID);

    int n = Players.size();

    for(int i = 0; i < n; i++){
        if(Players.at(i)->getID() == ID){
            Players.erase(Players.begin() + i);
            return;
        }
    }

}


void GameControl::sendAttention(int playerID, int attention){

    Utility::buildPacketHeader(packetID::Attention, packet);
    packet << playerID << attention;

    getPlayer(playerID)->setAttention(attention);

    for(auto& player : match->getPlayers()){
        Utility::sendPacket(player->getID(), packet);
    }

}

void GameControl::sendBlink(int playerID, int blinkStrenght){
    Utility::buildPacketHeader(packetID::Blink, packet);
    packet << playerID << blinkStrenght;

    getPlayer(playerID)->logBlink(blinkStrenght);
    getPlayer(playerID)->setBlinked(true);

    for(auto& player : match->getPlayers()){
        Utility::sendPacket(player->getID(), packet);
    }
}

void GameControl::sendGameStart(){
    Utility::buildPacketHeader(packetID::GameStart, packet);
    vector<Player*> players = getPlayers();

    for(auto& player : players){
        Utility::sendPacket(player->getID(), packet);
    }

}

void GameControl::sendChatMessage(int playerID, string msg){

    Utility::buildPacketHeader(packetID::Chat, packet);
    packet << playerID << msg;
    //Utility::sendPacket(toPlayer, packet);

}

void GameControl::createMatch(std::vector<Player*> leftTeam, std::vector<Player*> rightTeam){

    match = new Match(leftTeam, rightTeam);

    GameDAO::logMatch(match);

    sendGameStart();

}

void GameControl::endMatch(){

    if(match != nullptr){
        GameDAO::saveMatch(match);

        GameDAO::logPlayers(match);

        match->resetPlayers();

        delete match;

        match = nullptr;
    }

}


void GameControl::removeFromQueue(int playerID){
    int n = gameQueue.size();

    for(int i = 0; i < n; i++){
        if(gameQueue.at(i)->getID() == playerID){
            gameQueue.erase(gameQueue.begin() + i);
            return;
        }
    }
}


void GameControl::insertInQueue(Player* player){
    gameQueue.push_back(player);
    return;
}


void GameControl::insertInRoom(Player* player){
    gameRoom.insertInTeam(player);
}

void GameControl::removeFromRoom(int playerID){
    gameRoom.removeFromTeam(playerID);
}


void GameControl::gameRequest(int playerID){

}


void GameControl::sendGameEnd(int winner){

    if(match != nullptr){

        int teamOneAvg = static_cast<int>(match->getTeamOneAverageAttention());
        int teamTwoAvg = static_cast<int>(match->getTeamTwoAverageAttention());

        if(winner == 1){
            for(auto& player : match->getTeamOne()){
                Utility::buildPacketHeader(packetID::GameWin, packet);
                packet << static_cast<int>(player->getAverageAttention())
                       << teamOneAvg;
                Utility::sendPacket(player->getID(), packet);
            }


            for(auto& player : match->getTeamTwo()){
                Utility::buildPacketHeader(packetID::GameLose, packet);
                packet << static_cast<int>(player->getAverageAttention())
                       << teamTwoAvg;
                Utility::sendPacket(player->getID(), packet);
            }
        }
        else{

            for(auto& player : match->getTeamTwo()){
                Utility::buildPacketHeader(packetID::GameWin, packet);
                 packet << static_cast<int>(player->getAverageAttention())
                        << teamTwoAvg;
                Utility::sendPacket(player->getID(), packet);
            }

            for(auto& player : match->getTeamOne()){
                Utility::buildPacketHeader(packetID::GameLose, packet);
                 packet << static_cast<int>(player->getAverageAttention())
                        << teamOneAvg;
                Utility::sendPacket(player->getID(), packet);
            }
        }
        endMatch();
    }
}

void GameControl::setReady(int playerID, bool ready){

    getPlayer(playerID)->setReady(ready);

    if(allPlayersAreReady() and gameRoom.getTeamOne().size() > 0 and gameRoom.getTeamTwo().size() > 0){
        createMatch(gameRoom.getTeamOne(), gameRoom.getTeamTwo());
    }
}

bool GameControl::allPlayersAreReady(){
    vector<Player*> players = getPlayers();

    for(auto& player : players){
        if(not player->isReady()){
            return false;
        }
    }

    return true;
}


Player* GameControl::getPlayer(string nickname){
    for(auto& player : Players){
        if(player->getNickname() == nickname)
            return player;
    }
    return nullptr;
}

Player* GameControl::getPlayer(int ID){
    for(auto& player : Players){
        if(player->getID() == ID) return player;
    }
    return nullptr;
}

