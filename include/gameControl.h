#pragma once

#include <SFML/Network.hpp>
#include <SFML/System.hpp>

#include "match.h"
#include "serverControl.h"
#include "gameDAO.h"
#include "GameRoom.h"

#ifndef gameControl
#define gameControl GameControl::getInstance()
#endif

class Match;

class GameControl{

    public:
        static GameControl& getInstance();
        sf::Packet packet;

        Player* getPlayer(string nickname);
        Player* getPlayer(int ID);
        vector<Player*> getPlayers();
        void deletePlayer(int ID);

        Match* getMatch();

        void insertInQueue          (Player* player);
        void removeFromQueue        (int playerID);

        void insertInRoom           (Player* player);
        void removeFromRoom         (int playerID);

        void gameRequest            (int playerID);
        void sendAttention          (int playerID, int attention);
        void sendBlink              (int playerID, int blinkStrength);
        void sendGameStart          ();
        void sendGameEnd            (int winner);
        void sendChatMessage        (int playerID,  string msg);

        void createMatch            (vector<Player*> leftTeam, vector<Player*> rightTeam);
        void endMatch               ();

        void setReady(int playerID, bool ready);
        bool allPlayersAreReady();

    private:
        static GameControl* instance;
        GameControl();
        GameControl(GameControl const&);

        Match* match;
        GameRoom gameRoom;

        vector<Player*> Players;
        vector<Player*> gameQueue;

        int numberOfMatches;

};

