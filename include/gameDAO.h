#ifndef _GAMEDAO
#define _GAMEDAO
#include "match.h"
#include "configurationDAO.h"

class Match;
class Player;
class GameDAO{
    public:

        static void logPlayers(Match* match);

        static void logMatch(Match* match);

        static void saveMatch(Match* match);

};

#endif // _GAMEDAO
