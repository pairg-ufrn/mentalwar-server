#ifndef _MATCH
#define _MATCH

#include "player.h"
#include "utility.h"

class Player;

class Match{
    private:
        int ID;
        std::vector<Player*> teamOne;
        std::vector<Player*> teamTwo;

        int winner;
        int loser;

        int status;

    public:

        string startTime, endTime;

        std::vector<Player*> getTeamOne();
        std::vector<Player*> getTeamTwo();
        std::vector<Player*> getPlayers();

        float getTeamOneAverageAttention();
        float getTeamTwoAverageAttention();

        int getStatus();

        int getID();

        void setWinner(int winner);
        void setLoser(int loser);

        int getWinner();
        int getLoser();

        int getDuration();

        void resetPlayers();

        sf::Clock durationClock;

        Match(){};
        Match(std::vector<Player*> teamOne, std::vector<Player*> teamTwo);
        ~Match();

};

#endif // _MATCH
