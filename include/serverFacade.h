#ifndef _SERVERFACADE
#define _SERVERFACADE
#include "SFML/Network.hpp"
#include "dataType.h"
#include "serverControl.h"

class ServerFacade{
    public:
        static void handleRequest(sf::Packet packet, int pktID, int userID);
};

#endif // _SERVERFACADE
