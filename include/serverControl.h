#ifndef _SERVERCONTROL
#define _SERVERCONTROL

#define SEND_USERSLIST_DELAY_TIME 5
#define SEND_PLAYERSLIST_DELAY_TIME 16
#define SEND_MATCHESLIST_DELAY_TIME 12

#include "connectionHandler.h"
#include "utility.h"
#include "serverDAO.h"
#include "match.h"

class ServerControl{
    public:
        static void initialize();

        void broadcast   (std::string message);

        static int getNumberOfOnlineUsers();

        static void processLogin(int connectionID, std::string nickname, std::string version, int head, int body, int legs, std::string sex);

        static void sendPlayersList();

        static void notifyLogin(std::string nickname);

        static void connectUser(int ID, std::string nickname, int head, int body, int legs, std::string sex);
        static void disconnectUser(int ID);
        static void disconnectOfflineUsers();

    private:
        static int numberOfOnlineUsers;

        static ServerState state;

};

#endif // _SERVERCONTROL
