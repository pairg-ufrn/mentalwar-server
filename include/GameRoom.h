#ifndef GAMEROOM_H
#define GAMEROOM_H

#include<vector>
#include<player.h>

class GameRoom{
    public:
        GameRoom();
        virtual ~GameRoom();

        void insertInTeam(Player* _player);
        void insertInTeamOne(Player* _player);
        void insertInTeamTwo(Player* _player);

        void removeFromTeam(int playerID);

        vector<Player*> getTeamOne();
        vector<Player*> getTeamTwo();
        vector<Player*> getPlayers();

    private:

        vector<Player*> teamOne;
        vector<Player*> teamTwo;
};

#endif // GAMEROOM_H
