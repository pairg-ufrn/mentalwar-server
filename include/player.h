#ifndef _PLAYER
#define _PLAYER

#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <vector>
#include "dataType.h"
#include <iostream>

using namespace std;

class Player{
    public:
        std::string getNickname();

        int getID();

        float getAverageAttention();
        std::vector<int> getAttentionValues();

        statusID getStatus();
        bool isReady();
        bool hasBlinked();

        int getHead();
        int getBody();
        int getLegs();
        string getSex();

        void setID(int ID);
        void setNickname(std::string nickname);
        void setStatus(statusID status);
        void setReady(bool _ready);
        void setAttention(int att);
        void setBlinked(const bool& blink);
        void logBlink(const int& blinkStrength);

        void reset();

        Player();
        Player(int ID, std::string _nickname, int _head, int _body, int _legs, std::string _sex);

    private:

        statusID status;

        int ID;
        int head, body, legs;
        std::vector<int> attentionValues;

        string nickname;
        string sex;

        bool ready;
        bool blinked;
};

#endif // _PLAYER
