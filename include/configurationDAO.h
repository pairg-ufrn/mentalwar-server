#ifndef _CONFDAO
#define _CONFDAO
#include <iostream>
#include <map>
#include <fstream>
#include "utility.h"

#ifndef configurationDAO
#define configurationDAO ConfigurationDAO::getInstance()
#endif

class ConfigurationDAO{
    public:
        static ConfigurationDAO& getInstance();

        void loadConfiguration();
        void parse(std::ifstream & cfgfile);
        int getPort();
        std::string getLogDirectory();
        std::string getDataDirectory();
        int getTwitterModule();

    private:
        static ConfigurationDAO* instance;
        ConfigurationDAO(ConfigurationDAO const&);
        void operator=(ConfigurationDAO const&);


        int port;
        int _twitterModule;

        std::string logDirectory;
        std::string dataDirectory;
        std::map<std::string, std::string> options;

        ConfigurationDAO(){
            loadConfiguration();
        };


};

#endif // _CONFDAO
