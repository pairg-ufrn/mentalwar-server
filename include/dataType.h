#ifndef _DATA
#define _DATA

#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <sstream>

using namespace std;

const std::string GAME_VERSION = "0.1.9";

                    //0         1              2        3          4            5              6             7             8           9         10        11
enum class packetID {None,     Name,       Attention, Blink,    Connect,      Login,      LoginResponse, GameRequest,   GameEnd,   GameReady, GameStart, GameWin,
                     GameLose, Disconnect, Chat,      Response, WrongVersion, PlayerList, ExitQueue,     SpectatorChat, Broadcast, AlreadyConnected};

enum class masterPacketID {None, Login, LoginResponse, Disconnect, MatchList};

enum class statusID {Queue = 0, TeamOne = 1, TeamTwo = 2};

enum class ServerState {waiting, ongoingMatch};



#endif // _DATA
